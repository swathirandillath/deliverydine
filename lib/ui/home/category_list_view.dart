import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'model/category_model_list.dart';

class CategoryList extends StatefulWidget {
  List<MyList> _catList = [];
  Function(String) onTap;

  CategoryList(this._catList, this.onTap, {Key? key}) : super(key: key);

  @override
  _CategoryListState createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: widget._catList.length,
          itemBuilder: (context, index) {
            return Category(
                item: widget._catList[index],
                onTap: (catName) {
                  widget.onTap(catName);
                  widget._catList.asMap().forEach((i, v) {
                    if (v.name == catName) {
                      debugPrint("selected category : $catName}");
                      widget._catList[i].isSelected = true;
                    } else {
                      widget._catList[i].isSelected = false;
                    }
                  });
                });
          }),
    );
  }
}

class Category extends StatelessWidget {
  const Category({Key? key, required this.item, required this.onTap})
      : super(key: key);
  final MyList item;
  final Function(String) onTap;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        GestureDetector(
          onTap: () => onTap(item.name!),
          child: Column(
            children: [
              Padding(
                padding:
                    const EdgeInsets.only(left: 10.0, bottom: 10, right: 10),
                child: Container(
                  height: 100,
                  width: 150,
                  decoration: BoxDecoration(
                    border: item.isSelected!
                        ? Border.all(color: const Color(0xff29603F), width: 3)
                        : Border.all(color: const Color(0xffffffff), width: 3),
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                      image: AssetImage(item.imagePath!),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(left: 15.0, right: 10, bottom: 10),
                child: Text(
                  item.name.toString(),
                  style: const TextStyle(
                      fontSize: 15,
                      color: Color(0xff29603F),
                      fontWeight: FontWeight.w500),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
