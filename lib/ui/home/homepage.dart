import 'dart:async';
import 'dart:convert';

import 'package:assignment/ui/home/model/category_model_list.dart';
import 'package:assignment/ui/home/model/productlist_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

import 'category_list_view.dart';
import 'list_view.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  late final Future<String> _future;


  late ProductList _productList;
  late List<MyList> _catList = [];
  List<Items> _itemsCopy = [];

  // Fetch content from the json file
  Future<String> readJson() async {
    final String response = await rootBundle.loadString('assets/sample.json');
    final data = await json.decode(response);

    _productList = ProductList.fromJson(data);
    _itemsCopy = _productList.items!;

    debugPrint("size of product : ${_productList.items?.length}");

    if (_productList.items != null) {
      for (final product in _productList.items!) {
        MyList list = MyList();
        list.name = product.pCategory.toString();
        product.pCategory.toString() == 'Fruits'
            ? list.imagePath = 'assets/images/fruits_all.jpg'
            : product.pCategory.toString() == 'Vegetable'
                ? list.imagePath = 'assets/images/vegetables.jpg'
                : list.imagePath = 'assets/images/all_category.png';

        list.isSelected =false;

        if (_catList.isNotEmpty) {
          List<String> nameList = [];
          for (final cat in _catList) {
            nameList.add(cat.name!);
          }
          if (!nameList.contains(list.name)) {
            _catList.add(list);
          }
        } else {
          list.isSelected = true;
          _catList.add(list);
        }
      }
    }

    await Future.delayed(const Duration(seconds: 2));
    return Future.value("Data download successfully");
  }

  void filterProduct(String catName) {
    //clear data first
    _productList.items = [];

    if (catName == 'All') {
      setState(() {
        _productList.items = _itemsCopy;
      });
      debugPrint("product with cat others length: ${_productList.items?.length}");

      return;
    }

    List<Items> _newItems = [];
    for (final product in _itemsCopy) {
      if (product.pCategory == catName) {
        _newItems.add(product);
      }
    }

    setState(() {
      _productList.items = _newItems;
    });

    debugPrint("product with $catName length: ${_productList.items?.length}");

  }

  @override
  void initState() {
    super.initState();
    _future = readJson();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
            preferredSize: const Size.fromHeight(150.0),
            child: AppBar(
                title: const Center(
                    child: Text(
                  'welcome',
                  style: TextStyle(
                      fontSize: 18,
                      color: Color(0xff29603F),
                      fontWeight: FontWeight.bold),
                )),
                leading: IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.arrow_back,
                      color: Color(0xff29603F),
                    )),
                actions: [
                  IconButton(
                    icon: const Icon(Icons.shopping_bag_outlined,
                        color: Color(0xff29603F)),
                    tooltip: 'Open shopping cart',
                    onPressed: () {
                      // handle the press
                    },
                  ),
                ],
                backgroundColor: const Color(0xff97dc8b),
                automaticallyImplyLeading: false,
                // hides leading widget Color(0xff97dc8b)
                flexibleSpace: Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Container(
                          padding: const EdgeInsets.all(10),
                          height: 50,
                          width: double.infinity,
                          child: InkWell(
                            onTap: () {},
                            child: Row(
                              children: const [
                                Icon(
                                  Icons.search,
                                  color: Color(0xff29603F),
                                ),
                                Text(
                                  "search",
                                  style: TextStyle(
                                      color: Color(0xff29603F),
                                      fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                          ),
                          decoration: BoxDecoration(
                              border:
                                  Border.all(color: const Color(0xff29603F)),
                              borderRadius: BorderRadius.circular(15)),
                        ))))),
        body: SingleChildScrollView(
            child: ConstrainedBox(
                constraints: BoxConstraints(
                    maxHeight: MediaQuery.of(context).size.height),
                child: FutureBuilder<String>(
                    future: _future,
                    builder:
                        (BuildContext context, AsyncSnapshot<String> snapshot) {
                      List<Widget> children;
                      if (snapshot.hasData) {
                        children = <Widget>[
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding: EdgeInsets.only(
                                    left: 15.0, top: 50, bottom: 15),
                                child: Text(
                                  "Categories",
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Color(0xff29603F),
                                      fontWeight: FontWeight.bold),
                                ),
                              )),
                          CategoryList(_catList, (catName) {
                            debugPrint("On Click : CategoryName: $catName");
                            filterProduct(catName);
                          }),
                          const Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding: EdgeInsets.only(
                                    left: 15.0, bottom: 15),
                                child: Text(
                                  "Items",
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Color(0xff29603F),
                                      fontWeight: FontWeight.bold),
                                ),
                              )),
                          Expanded(child: ListViewPage(_productList.items!)),
                        ];
                      } else if (snapshot.hasError) {
                        children = <Widget>[
                          const Icon(
                            Icons.error_outline,
                            color: Colors.red,
                            size: 60,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 16),
                            child: Text('Error: ${snapshot.error}'),
                          )
                        ];
                      } else {
                        children = <Widget>[
                          Center(
                            child: Column(
                              children: [
                                SizedBox(
                                  width: 20,
                                  height: 20,
                                  child: CircularProgressIndicator(),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 16),
                                  child: Text('loading shop...'),
                                )
                              ],
                            ),
                          ),
                        ];
                      }
                      return Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: children,
                        ),
                      );
                    }))));
  }
}
