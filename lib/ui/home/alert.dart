import 'package:flutter/material.dart';

import 'model/productlist_model.dart';

class CustomDialog extends StatefulWidget {
  CustomDialog(this.item, this.onTap, {Key? key}) : super(key: key);
  final Function(int) onTap;

  final Items item;

  @override
  State<CustomDialog> createState() => _CustomDialogState();
}

class _CustomDialogState extends State<CustomDialog> {
  TextEditingController _controller = TextEditingController();

  bool _showError = false;

  dialogContent(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(10),
        boxShadow: const [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: Offset(0.0, 10.0),
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min, // To make the card compact
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const SizedBox(height: 24.0),
                Text(
                  'Name : ' + widget.item.pName!,
                  style: const TextStyle(
                    fontSize: 15.0,
                    color: Color(0xff29603F),
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(height: 16.0),
                Text(
                  'Stock : ' + widget.item.pAvailability.toString(),
                  style: const TextStyle(
                    fontSize: 15.0,
                    color: Color(0xff29603F),
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(height: 16.0),
                Text(
                  'Description : ' + widget.item.pDetails!,
                  style: const TextStyle(
                    fontSize: 15.0,
                    color: Color(0xff29603F),
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(height: 16.0),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 8, 30, 8),
                  child: TextField(
                    controller: _controller,
                    keyboardType: TextInputType.number,
                    cursorColor: Color(0xff1E7248),
                    decoration: InputDecoration(
                        errorText:
                            _showError ? 'Qty is greater thar stock' : null,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                          borderSide: BorderSide(width: 1, color: Colors.green),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                          borderSide:
                              BorderSide(width: 1.5, color: Colors.green),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        ),
                        hintText: 'Enter Quantity',
                        hintStyle: TextStyle(
                          color: Color(0xff29603F),
                        )),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 24.0),
          Align(
            alignment: Alignment.bottomCenter,
            child: InkWell(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                child: Container(
                  height: 50,
                  width: 100,
                  decoration: BoxDecoration(
                    gradient: const LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      colors: [
                        Color(0xff4aaf5f),
                        Color(0xff6ed083),
                      ],
                    ),
                    color: const Color(0xff96C3A7),
                    border: Border.all(color: const Color(0xff1E7248)),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: const Center(
                      child: Text(
                    "Submit",
                    style: TextStyle(
                        fontSize: 18,
                        color: Color(0xff29603F),
                        fontWeight: FontWeight.bold),
                  )),
                ),
              ),
              onTap: () {
                if (int.parse(_controller.text) > widget.item.pAvailability!) {
                  setState(() {
                    _showError = true;
                  });
                } else {
                  widget.onTap(int.parse(_controller.text));
                  Navigator.pop(context);
                }
              },
            ),
          ),
          const SizedBox(height: 10.0),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }
}
