class ProductList {
  List<Items>? items;

  ProductList({this.items});

  ProductList.fromJson(Map<String, dynamic> json) {
    if (json['items'] != null) {
      items = <Items>[];
      json['items'].forEach((v) {
        items!.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.items != null) {
      data['items'] = this.items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  String? pName;
  int? pId;
  int? pCost;
  int? pAvailability;
  int? pCount;
  String? pImage;
  String? pDetails;
  String? pCategory;

  Items(
      {this.pName,
        this.pId,
        this.pCost,
        this.pAvailability,
        this.pCount,
        this.pImage,
        this.pDetails,
        this.pCategory});

  Items.fromJson(Map<String, dynamic> json) {
    pName = json['p_name'];
    pId = json['p_id'];
    pCost = json['p_cost'];
    pAvailability = json['p_availability'];
    pCount = json['p_count'];
    pImage = json['p_image'];
    pDetails = json['p_details'];
    pCategory = json['p_category'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['p_name'] = this.pName;
    data['p_id'] = this.pId;
    data['p_cost'] = this.pCost;
    data['p_availability'] = this.pAvailability;
    data['p_count'] = this.pCount;
    data['p_image'] = this.pImage;
    data['p_details'] = this.pDetails;
    data['p_category'] = this.pCategory;
    return data;
  }
}
