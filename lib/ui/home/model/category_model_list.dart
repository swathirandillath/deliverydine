class MyList {
  String? name;
  String? imagePath;
  bool? isSelected;

  MyList({this.name, this.imagePath,this.isSelected});
}