import 'package:assignment/ui/home/model/productlist_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'alert.dart';

class ListViewPage extends StatefulWidget {

  List<Items> items = [];

  ListViewPage(this.items, {Key? key}) : super(key: key);


  @override
  _ListViewPageState createState() => _ListViewPageState();
}

class _ListViewPageState extends State<ListViewPage> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount:  widget.items.length,
        itemBuilder: (context, index) {
          return ListPage( widget.items[index]);
        });
  }
}

class ListPage extends StatefulWidget {
  ListPage(this.item, {Key? key}) : super(key: key);

  final Items item;

  @override
  State<ListPage> createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shadowColor:Colors.green,
      margin: const EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(
            children: [
              Image.asset(
                widget.item.pImage.toString(),
                height: 100,
                width: 100,
              )
            ],
          ),
          SizedBox(width: 150,
            child: Column(
               mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(widget.item.pName!),
                Text(widget.item.pDetails!),
                Text('₹' + widget.item.pCost.toString())
              ],
            ),
          ),
          // SizedBox(width: 150),
          InkWell(
            child: Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                gradient: const LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [
                    Color(0xff4aaf5f),
                    Color(0xff6ed083),
                  ],
                ),
                color: const Color(0xff96C3A7),
                border: Border.all(color: const Color(0xff1E7248)),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Center(
                  child: Text(
                    widget.item.pCount == 0
                    ? "Add"
                    :  widget.item.pCount.toString(),
                style: const TextStyle(
                    fontSize: 18,
                    color: Color(0xff29603F),
                    fontWeight: FontWeight.bold),
              )),
            ),
            onTap: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return CustomDialog(widget.item , (count){
                      setState(() {
                        widget.item.pAvailability = widget.item.pAvailability! - count;
                        widget.item.pCount =  widget.item.pCount! + count;
                      });
                    });
                  });
            },
          ),
        ],
      ),
    );
  }
}
